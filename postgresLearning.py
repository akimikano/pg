import psycopg2
from psycopg2 import Error

connection = psycopg2.connect(user="poycfbwz",
                              password="ISjj-LmMaENfMauT045qsIkuAZgw02-4",
                              host="queenie.db.elephantsql.com",
                              database="poycfbwz")


def get_max_in_dict(dic): # returns key of maximum total weight in dictionary
    m = max(dic.values())
    ans_dic = {key: value for key, value in dic.items() if value == m}
    return ans_dic


def get_conflict_query(post_id, text, dic):
    conflict_detail = ', '.join([f'{key} - {value}' for key, value in dic.items()])
    query = "INSERT INTO morphs_in_posts (post_id, morphs, has_conflicts, conflicts, no_morphs) VALUES (%s, '%s', %s, '%s', %s)" % (post_id, text, True, conflict_detail, False)
    return query


def get_non_conflict_query(post_id, category_id, text):
    query = "INSERT INTO morphs_in_posts (post_id, category_id, morphs, has_conflicts, no_morphs) VALUES (%s, %s, '%s', %s, %s)" % (post_id, category_id, text, False, False)
    return query


def get_query(hundreds, non_hundreds, text, post_id): # return query string for inserting
    if hundreds:
        dic = get_max_in_dict(hundreds)
        if len(dic) > 1:
            query = get_conflict_query(post_id, text, dic)
            return query
        else:
            category_id = list(dic)[0]
            query = get_non_conflict_query(post_id, category_id, text)
            return query
    elif non_hundreds:
        dic = get_max_in_dict(non_hundreds)
        if len(dic) > 1:
            query = get_conflict_query(post_id, text, dic)
            return query
        else:
            category_id = list(dic)[0]
            query = get_non_conflict_query(post_id, category_id, text)
            return query
    return "INSERT INTO morphs_in_posts (post_id, no_morphs) VALUES (%s, %s)" % (post_id, True)


def analyze_post_text(morphs, post): # searchs for morphs in post text
    hundreds = {}
    non_hundreds = {}
    text = ''
    post_id = post[0]
    post_text = ('%s %s' % (post[1]['post_text'], post[1]['group_name'])).lower()

    for i in range(len(morphs)):
        morph = morphs[i][1].lower()
        count = post_text.count(morph)
        if count > 0:
            if morphs[i][2] < 100:
                if hundreds:
                    text += f'{morphs[i][1]}({count})'
                    continue
                total_weight = count * morphs[i][2]
                non_hundreds[morphs[i][0]] = non_hundreds.get(morphs[i][0], 0) + total_weight
                text += f'{morphs[i][1]}({count}) '
                continue
            total_weight = count * morphs[i][2]
            hundreds[morphs[i][0]] = hundreds.get(morphs[i][0], 0) + total_weight
            text += f'{morphs[i][1]}({count}) '

    insert_query = get_query(hundreds, non_hundreds, text, post_id)

    return insert_query


def main():
    try:
        cursor = connection.cursor()
        print('Информация о подключении')
        print(connection.get_dsn_parameters(), "\n")
        cursor.execute("SELECT version();")
        record = cursor.fetchone()
        print("Вы подключены к базе - ", record, "\n")
        query = 'select * from groups order by id'
        cursor.execute(query)
        posts = cursor.fetchall()

        query = 'select category_id, category_morph, category_morph_weight from morphs'
        cursor.execute(query)
        morphs = cursor.fetchall()

        for post in posts:
            insert_query = analyze_post_text(morphs, post)
            # print(insert_query)
            cursor.execute(insert_query)
            connection.commit()
            cursor.execute("UPDATE groups SET was_checked = %s WHERE id = %s" % (True, post[0]))
            connection.commit()

    except (Exception, Error) as error:
        print("Возникла ошибка при подключении к базе: ", error)

    connection.close()


if __name__ == '__main__':
    main()
